import requests
import json
import sqlite3
from itertools import groupby
from operator import itemgetter
from mako.template import Template

#gets the data from the api
response = requests.get("https://gorest.co.in/public-api/categories")

data = response.json()

#gets the list of data from data
dataList = data["data"]

#creates connection to project.db
conn = sqlite3.connect('project.db')

c = conn.cursor()

#drops the table and creates a new one to reset the table
c.execute("DROP TABLE categories")
c.execute("""
    CREATE TABLE categories(
    id integer,
    name text,
    description text,
    status integer,
    calculation real
    )
""")

#inserts each dictionary into the table
for d in dataList:
    dataDict = d
    #calculates the id/id-5 field
    calcSolution = dataDict["id"]
    calcDenominator = dataDict["id"] - 5
    #makes sure there is no division by 0 otherwise defaults to division by 1
    if calcDenominator == 0:
        calcDenominator = 1
    calcSolution = calcSolution/calcDenominator
    c.execute("INSERT INTO categories (id, name, description, status, calculation) VALUES(?,?,?,?,?)",(dataDict["id"],dataDict["name"],dataDict["description"],dataDict["status"],calcSolution))

#retrieves data from database and category table
c.execute("SELECT * FROM categories")
list = c.fetchall()

#creates an html page to display the data retrieved from the database
t = Template(filename='index_template.html')

file = open("index.html","w")
file.write(t.render(data=groupby(sorted(list), itemgetter(0))))
file.close()
conn.close()
