## Welcome to the ItGISworx recruitment project! ##

### What is this repository for? ###
------------------------------------

This repo serves to test knowledge of web technologies for people wanting to join our team.  
The mini project outlined below seeks to test skills in aspects of full stack web development, including:

* data retrieval from an API
* data storage in a database
* data presentation
* troubleshooting


### How do I get set up? ###
----------------------------
#### Create Bitbucket account
If you already don't have bitbucket account, create one [here](https://bitbucket.org/account/signup/)

#### Clone this repo 
```bash
git clone https://itGISworx@bitbucket.org/itgisworxdev/starfleet.git
```
#### Make a new branch using your name
```bash
git checkout -b <YourName>
```
#### Update your git config to use your bitbucket user's details
```bash
git config --global user.email "your@email.com"
```
```bash
git config --global user.name "YourName"
```


### Mini Project ###
--------------------
```bash
echo "Your mission, commander, should you choose to accept it..."
```

#### Get the data
* Write a backend service to pull data from [https://gorest.co.in/public-api/categories](https://gorest.co.in/public-api/categories)
* Store the retrieved data in a sqlite database.

#### Present the data
* Retrieve data from the sqlite database
* Present in web-based (html) frontend.  
* In the data presented, add an additional field that shows the result of each record's ```id/(id-5)```

#### Upload your solution in and tell us about it
* Create a README file with instructions for installation/usage of your awesome solution (We need to be able to get it up and running our side to see!)
* Commit and push your code to the git branch you created.
* Drop us a mail at info@itgisworx.co.za

For the backend, you're welcome to implement the server-side component in any language of your choice e.g. Python, C#, Java, Perl, .net, Rails etc.  
(Browny points for using Python or Perl)

For the web-frontend, you're welcome to use any web-tech e.g. Vue, JQuery, Bootstrap, React etc.